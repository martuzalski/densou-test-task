# Small test task for Densou Trading Desk

### Introduction

We have a couple of things that we would want to feel you out on.

 1. How well do you handle popular frameworks? Do you use built-in functionality or do you write functionality yourself?
 2. Architecture. Do you have a gut feeling for scaling applications.
 3. Current standards and best practice. Do you use them, and is your code easy for other developers to debug and maintain.

### What do we have?

This is an almost empty PhalconPHP skeleton project. Information about the framework can be found here https://phalconphp.com/

There is a page with a form, in where you can fill in text, and a submit button.

### What do we want?

1. Coding: When you type in a github repo, for example "laravel/laravel" we want all the contributors to that repo, listed below with their name, picture (avatar) and number of contributions.  
    1.  It should be possible to sort the contributors, by contributions or name.
    2.  Also we want recent searches saved in a database. This list should be shown on a different page.
2. Writing: Write 10 lines about how this application could support fetching data from more than GitHub. Let us say we wanted to fetch data from Stack Overflow and LinkedIn too. How should this be done with tables in a database, organising code in classes in the project, etc.

### How we want it?

Since others should also have the pleasure of this project, please do not submit anything to it.
Feel free to download it, and create your own copy for submission.
Thank you.
