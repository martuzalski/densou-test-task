<div class="page-header">
    <h1>Github contributor lookup</h1>
</div>

{{ form() }}
{{ text_field('repository') }}
{{ submit_button('Look up contributors') }}
{{ end_form() }}


<ul class="contributors">
    <li class="contributor">
        <a>
            <img src="https://avatars2.githubusercontent.com/u/1465?v=3">
            <span class="contributor-name">name1</span>
            <span class="contributor-contributions">12</span>
        </a>
    </li>
    <li class="contributor">
        <a>
            <img src="https://avatars2.githubusercontent.com/u/1955?v=3">
            <span class="contributor-name">name2</span>
            <span class="contributor-contributions">12</span>
        </a>
    </li>
    <li class="contributor">
        <a>
            <img src="https://avatars2.githubusercontent.com/u/8959?v=3">
            <span class="contributor-name">name3</span>
            <span class="contributor-contributions">12</span>
        </a>
    </li>
</ul>

<ul>
    <li>
        <span class="repo-link">{{ link_to('https://github.com/laravel/laravel', 'laravel/laravel') }}</span>
        <span class="search-time">Wed May 17 11:41:07 UTC 2017</span>
    </li>
    <li>
        <span class="search-string"></span>
        <span class="repo-link">{{ link_to('https://github.com/phalcon/zephir', 'phalcon/zephir') }}</span>
        <span class="search-time">Wed May 17 11:58:51 UTC 2017</span>
    </li>

</ul>